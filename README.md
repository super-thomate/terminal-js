# README #

### What is this thing for? ###

* Create a terminate-like display for your datas.
* This is v.1.0.0

### How do I use it ? ###
* Take a DOM element of your choice (<section> or <article> make more sense, but <div> is fine to)
* Create a new instance of Terminal like that : `const terminal = new Terminal("console")`, where `console` is the id of your DOM element.
* Enjoy by adding any line like that : `terminal.log(myNewLine[, myNewLineType])`, where `myNewLineType` is either `"debug"`, `"success"`, `"warning"`, or `"error"`
