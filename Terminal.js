/**
 * Terminal.js
 * Create a terminate-like display for your datas.
 * Version : 1.1.0
 * Copyright : Super-Thomate 2017
 */
class Terminal {
    constructor(id, options={
    	name: "default",
    	img: null,
    	debug: true,
    	autoScroll: true
    }){
        // OPTIONS
        this.setName(options.name);
        this.setImage(options.img);
        this.setDebug(options.debug);
    	this.setId(id);
    	this.setAutoScroll(options.autoScroll);
    	// INITIALIZE
    	this.setLineNumber(0);
    	// ELEMENT
    	this.parentElement = document.querySelector("#"+id);
        try {
        	this.parentElement.classList.add("terminal-js");
        	this.parentElement.innerHTML = `
	    	<div id="${this._id}-head" class="terminal-head-js">
			</div>
			<div id="${this._id}-body" class="terminal-body-js">
				
			</div>
	    	`;
	    	this.addHead();
        } catch (error) {
        	console.error(error);
        } 
    }
    /**
     ** SETTERS & GETTERS 
     */
    setId(id){
    	this._id = id;
    	return this;
    }
    getId(){
    	return this._id;
    }
    
    setName(name){	
    	this._name = typeof(name) === "string" ? name : "Application Unnamed";
    	return this;
    }
    getName(){
    	return this._name;
    }
    
    setImage(img){
    	this._img = typeof(img) === "string" ? img : null;
    	return this;
    }
    getImage(){
    	return this._img;
    }
    
    setDebug(bool){
    	this._debug = typeof(bool) === "boolean" ? bool : true;
    	return this;
    }
    getDebug(){
    	return this._debug;
    }
    
    setLineNumber(number){
    	this._number = typeof(number) === "number" ? number : this._number;
    	return this;
    }
    getNumber(){
    	return this._number;
    }
    
    setAutoScroll(bool){
    	this._autoScroll = typeof(bool) === "boolean" ? bool : true;
    	return this;
    }
    getAutoScroll(){
    	return this._autoScroll;
    }
    /**
     ** METHODS
     */
    addHead(){
    	if(this._img != null){
    		document.querySelector(`#${this._id}-head`).innerHTML = `
    		<img src="${this._img}" alt="${this._name}"/><p>${this._name}</p>
	    	`;
    	} else {
    		document.querySelector(`#${this._id}-head`).innerHTML = `
    		<p>${this._name}</p>
	    	`;
    	}
    }
    log(line, type=""){
    	let lineClass = "";
    	let prefix = "";
    	switch (type) {
    		case 'success':
    			lineClass += "terminal-line-success";
    			prefix += "[SUCCESS] ";
    			break;
    		case 'warning':
    			lineClass += "terminal-line-warning";
    			prefix += "[WARNING] ";
    			break;
    		case 'error':
    			lineClass += "terminal-line-error";
    			prefix += "[ERROR] ";
    			break;
    		case 'debug':
    			lineClass += this._debug ? "terminal-line-debug" : "terminal-line-debug-hidden";
    			prefix += "[DEBUG] ";
    			break;
    		default:
    			// Just a regular line, no formatting
    	}
    	try{
    		document.querySelector(`#${this._id}-body`).innerHTML += `
    		<p class="${lineClass}" data-lineNumber="${this.getNumber()}">${prefix}${line}</p>
    		`;
    		this.setLineNumber(this.getNumber()+1);
    		if(this.getAutoScroll())
	    		document.querySelector(`#${this._id}-body`).scrollTop = document.querySelector(`#${this._id}-body`).scrollHeight;
    	} catch(error) {
    		console.error(error);
    	}
    }
    /**
     ** STATICS METHODS
     */
    static addCSS(){
    	document.head.innerHTML += `
        	<style type="text/css">
				.terminal-js {
					box-sizing: border-box;
					background: #000;
					color: #fff;
					font-family: monospace;
					width: 80%;
					margin: 0 auto 1rem auto;
					box-shadow: 0px 0px 5px 4px rgba(0, 0, 0, 0.4);
				}
				.terminal-js p{
					margin: 0;
					padding-left: 5px;
				}
				.terminal-js .terminal-head-js {
					background: #fafafa;
					padding-left: 5px;
					color: #000;
					height: 30px;
				}
				.terminal-js .terminal-head-js p {
					display: inline-block;
					width: 50%;
					vertical-align: middle;
				}
				.terminal-js .terminal-head-js img{
					max-width: 24px;
					max-height: 24px;
				}
				.terminal-js .terminal-body-js {
					height: 450px;
					overflow-y: scroll;
					padding: 0 5px;
				}
				.terminal-js .terminal-head-js,
				.terminal-js .terminal-body-js {
					border: 2px solid #fafafa;
				}
				.terminal-line-success{
					color: #28a745;
				}
				.terminal-line-warning{
					color: #ffc107;
				}
				.terminal-line-error{
					color: #dc3545;
				}
				.terminal-line-debug{
					color: #17a2b8;
				}
				.terminal-line-debug-hidden{
					display: none;
				}
			</style>
        	`;
    }
    static create(id, options={}){
    	new Terminal(id, options);
    }
}
// ADD CSS TO THE HEAD
Terminal.addCSS();